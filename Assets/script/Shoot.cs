﻿using UnityEngine;
using System.Collections;

public class Shoot : MonoBehaviour {
	
	public GameObject _bullet;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetButtonDown("Fire1"))
		{
			GameObject clone;
			clone = Instantiate(_bullet, new Vector3(transform.position.x,transform.position.y,transform.position.z + 1.5f), transform.rotation) as GameObject;// 객체 복사 
			clone.rigidbody.AddForce(Vector3.forward * 500);
		}
	}
	
}