﻿using UnityEngine;
using System.Collections;

public class Score : MonoBehaviour {
	
	public int _Score;
	public GUIText print;
	// Use this for initialization
	void Start () {
	_Score = 0;
	}
	
	// Update is called once per frame
	void Update () {
		print.text = "Score : " + _Score.ToString();
	}
}
